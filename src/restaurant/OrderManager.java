/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import com.sun.javafx.collections.ObservableListWrapper;
import java.util.LinkedList;
import javafx.collections.ObservableList;
import menu.Meal;
import menu.Order;

/**
 *
 * @author Admin
 */
public class OrderManager {
    
    private Order order;

    public OrderManager(Order theOrder) {
        if (theOrder == null) throw null;
        this.order = theOrder;
    }
    
    public final Order getOrder(){
        return this.order;
    }
    
    public ObservableList<Meal> getCommonMeals(){
        LinkedList<Meal> meals = new LinkedList<Meal>();
        for (Meal m : order.common){
            meals.add(m);
        }
        return new ObservableListWrapper<Meal>(meals);
    }
    
    public ObservableList<Meal> getAllOrderMeals(){
        return new ObservableListWrapper<Meal>(order.allMeals());
    }
}
