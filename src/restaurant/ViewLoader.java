/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import menu.Meal;
import menu.Person;

/**
 *
 * @author Admin
 */
public class ViewLoader {
    public static Node loadNewLittleMealNode(final Meal m, Person owner, Pane parent) {
        Node mealNode = null;
        try {
            URL location = ViewLoader.class.getResource("little_meal.fxml");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(location);
            mealNode = (Node) loader.load(ViewLoader.class.getResourceAsStream("little_meal.fxml"));
            LittleMealController littleController = (LittleMealController) loader.getController();
            littleController.init(owner, parent);
            littleController.initWithMeal(m);
            littleController.initDragAndDrop();
            
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return mealNode;
        }
    }
}
