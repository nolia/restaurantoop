/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import menu.Meal;

/**
 *
 * @author Admin
 */
public class MealInComboController extends AbstractMealController{

    @FXML private Label labelName;
    @FXML
    Rectangle rectImage;
    @FXML
    AnchorPane anchorRoot;
    
    

    @Override
    public void initWithMeal(Meal meal) {
        labelName.setText(meal.getName());
        setMealImage(meal, rectImage, anchorRoot);
    }
    
}
