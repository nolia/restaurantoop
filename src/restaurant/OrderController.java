/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.IOException;
import menu.util.OrderListener;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import menu.Meal;
import menu.Order;
import menu.Person;
import menu.RestaurantMenu;

/**
 *
 * @author Admin
 */
public class OrderController implements Initializable, OrderListener{
    private FXMLLoader loader = new FXMLLoader();
    private OrderManager orderManager;
    
    @FXML private Label labelPrice;
    @FXML private GridPane gridTable;
    @FXML private ListView<Meal> listViewCommon;
    @FXML private VBox vboxCommon;
    
    private Order order;
    // indicates 1-began index
    private int gnumber = 0;
    private Node[] personViews = new Node[RestaurantMenu.MAX_GUESTS_NUMBER];
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        order = JavaFXMenu.getInstance().getOrder();
        order.addListener(this);
        loadInits();
    }
    
    public void onToMenu(ActionEvent event){
        JavaFXMenu.getInstance().goToMenu();
    }

    @Override
    public void onOrderUpdated() {
        String summ = String.format("$ %.2f", order.total());
        labelPrice.setText(summ);
    }

    private void loadInits() {
        vboxCommon.setMargin(vboxCommon, Insets.EMPTY);
        for (Meal m : order.common.getMealsSet()){
            vboxCommon.getChildren().add(
                    //new Label(m.getName()));
                    ViewLoader.loadNewLittleMealNode(m, order.common, vboxCommon));
        }
        vboxCommon.setAlignment(Pos.CENTER_LEFT);
        // guests
        int count = order.getGuestsNumber();
        for (int pNumber = 1; pNumber <= count; ++pNumber){
            Person p = order.getGuest(pNumber);
            int num = pNumber - 1;
            Node content = loadPerson(p, num);
            int col, row;
            col = num / 3;
            row = num % 3;
            gridTable.add(content, col, row);
        }
        gnumber = count;
    }
    
    
    private void clear() {
        
    }
    
    public void onPlusGuest(ActionEvent event){
        if (gnumber < RestaurantMenu.MAX_GUESTS_NUMBER){
            addNewGeuest();
        }
    }
    
    public void onMinusGuest(ActionEvent event){
        if (gnumber > 0){
            gridTable.getChildren().remove(personViews[gnumber - 1]);
            gnumber = order.removeGuest();
        }
    }

    private void addNewGeuest() {
            gnumber = order.addGuest();
            Person p = order.getGuest(gnumber);
            Node content = loadPerson(p, gnumber - 1);
            
            int n = gnumber - 1;
            int col, row;
            col = n / 3;
            row = n % 3;
            gridTable.add(content, col, row);
    }

    private Node loadPerson(Person p , int personNumber) {
        // load new pesonView:
        FXMLLoader loader = new FXMLLoader();
        Node content = null;
        try {
            //loader.setLocation(getClass().getResource("person_view.fxml"));
            content = (Node) loader.load(getClass().getResourceAsStream("person_view.fxml"));
            personViews[personNumber] = content;
            PersonViewController controller = (PersonViewController) loader.getController();
            
            controller.initWithPerson(p);
        } catch (IOException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return content;
        }
    }
}
