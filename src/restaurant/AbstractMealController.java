/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import menu.Meal;

/**
 *
 * @author Admin
 */
public abstract class AbstractMealController {

    public AbstractMealController() {
    }

    public abstract void initWithMeal(Meal meal);

    public ImageView setMealImage(Meal meal, Rectangle imageRect, AnchorPane anchorRoot) {
        InputStream imageStream = null;
        ImageView imageView = null;
        try {
            String imagePath = meal.getImageName();
            if (imagePath == null) {
                return null;
            }
            imageStream = new FileInputStream(imagePath);
            Image mealImage = new Image(imageStream);
             imageView = new ImageView(mealImage);
            imageView.setX(imageRect.getX());
            imageView.setY(imageRect.getY());
            imageView.setFitHeight(imageRect.getHeight());
            imageView.setFitWidth(imageRect.getWidth());
            imageView.setLayoutX(imageRect.getLayoutX());
            imageView.setLayoutY(imageRect.getLayoutY());
            anchorRoot.getChildren().add(imageView);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MealController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (imageStream != null) {
                    imageStream.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(MealController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return imageView;
        }
    }
    
}
