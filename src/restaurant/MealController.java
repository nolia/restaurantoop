/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import menu.ExtraInfo;
import menu.Meal;
import menu.Menu;
import menu.Order;

/**
 *
 * @author Admin
 */
public class MealController extends AbstractMealController implements Initializable, MealOrderControlls{

    @FXML
    Label labelMealTitle;
    @FXML
    Label labelPrice;
    // btn to show detailed description:
    @FXML
    Button btnExtra;
    @FXML
    Button btnPlusMeal;
    @FXML
    Button btnNumber;
    @FXML
    Button btnMinus;
    @FXML
    Button btnDelete;
    private Order order = JavaFXMenu.getInstance().getOrder();
    private Tooltip toolTipDescr;
    private Meal theMeal;
    @FXML
    Rectangle rectImage;
    @FXML
    AnchorPane anchorRoot;

    public void setTitle(String title) {
        labelMealTitle.setText(title);
    }

    @Override
    public void onPlusMeal(ActionEvent event) {
        order.addMeal(theMeal);
        update();
    }
    
    @Override
    public void onMinusMeal(ActionEvent event){
        order.reduceMeal(theMeal);
        update();
    }
    @Override
    public void onDelete(ActionEvent event){
        order.deleteAllMeals(theMeal);
        update();
    }

    @Override
    public void initWithMeal(Meal meal) {
        this.theMeal = meal;
        labelMealTitle.setText(meal.getName());
        initControlButtons();
        setMealImage(meal, rectImage, anchorRoot);
        labelPrice.setText("$" + String.valueOf(meal.getPrice()));
        String descr = meal.getDescription();
        if (descr != null) {
            toolTipDescr = new Tooltip(descr);
            labelMealTitle.setTooltip(toolTipDescr);
        }
        Collection<ExtraInfo> extras = Menu.getInstance().extraInfoList.getSuitable(meal);
        if (extras != null && !extras.isEmpty()) {
            ContextMenu extraMenu = new ContextMenu();
            for (ExtraInfo info : extras) {
                extraMenu.getItems().add(new MenuItem(info.getName()));
            }
            btnExtra.setContextMenu(extraMenu);
        } else {
            btnExtra.setVisible(false);
        }
    }

    public void update(){
        initControlButtons();
        order.update();
    }
    
    private void initControlButtons() {
        int q = order.getMealsNumber(theMeal);
        boolean toAddControls = q > 0;
        btnDelete.setVisible(toAddControls);
        btnNumber.setVisible(toAddControls);
        btnMinus.setVisible(toAddControls);
        if (toAddControls) {
            btnNumber.setText(String.valueOf(q));
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        order = JavaFXMenu.getInstance().getOrder();
    }
}
