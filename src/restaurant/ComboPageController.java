/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import menu.ComboMeal;
import menu.Meal;
import menu.util.FXMLWrapper;

/**
 *
 * @author Admin
 */
public class ComboPageController implements Initializable{
    
    @FXML private GridPane gridMeals;
    @FXML private Label labelComboName1;
    @FXML private Label labelComboName2;
    @FXML private Label labelPrice1;
    @FXML private Label labelPrice2;

    
    private Iterator<ComboMeal> comboIter;
    private ComboMeal firstCombo;
    private FXMLWrapper loaderWrapper = new FXMLWrapper();
    private ComboMeal secondCombo;
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        ////
    }

    public void initWithCombo(Collection<ComboMeal> category) {
        comboIter = category.iterator();
        if (comboIter.hasNext()) firstCombo = comboIter.next();
        if (comboIter.hasNext()) secondCombo = comboIter.next();
        
        setCombo(firstCombo, 0);
        setCombo(secondCombo, 1);
    }

    private void setCombo(ComboMeal combo, int col) {
        if (combo == null){
            return;
        }
        setLabels(combo, col);
        Iterator<Meal> mealIter = combo.iterator();
        int i = 0;
        for (Iterator<Meal> meal = combo.iterator(); meal.hasNext();++i) {
            Meal m = meal.next();
            Node content = buildNewMeal(m);
            
            gridMeals.add(content, col, i);
            
                    
        }
    }

    private Node buildNewMeal(Meal m) {
        Node content = null;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("meal_in_combo.fxml"));
            content = (Node) loader.load(getClass().getResourceAsStream("meal_in_combo.fxml"));
            MealInComboController controller = (MealInComboController) loader.getController();
            controller.initWithMeal(m);
            
        } catch (IOException ex) {
            Logger.getLogger(ComboPageController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return content;
        }
    }

    private void setLabels(ComboMeal combo, int col) {
        switch (col){
            case 0:
                labelComboName1.setText(combo.getName());
                labelPrice1.setText(combo.getPrice());
                break;
            case 1:
                labelComboName2.setText(combo.getName());
                labelPrice2.setText(combo.getPrice());
                break;
        }
        
    }
    
    
}
