/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import menu.*;
import menu.Menu;
import menu.util.FXMLWrapper;
import menu.util.OrderListener;

/**
 *
 * @author Admin
 */
public class MainMenuCotroller implements Initializable, OrderListener {
    
    @FXML private Button btnExit;
    @FXML private Button btnToAdmin;
    private ComboMealPool comboPool = menu.Menu.getInstance().comboMealList;
    private FXMLLoader fxmlLoader = new FXMLLoader();
    @FXML private TabPane tabPaneMenu;

    private Order order;
    @FXML private Label labelPrice;
    
    public void onClickExit(ActionEvent event){
        System.exit(0);
    }
    
    private Tab buildTabCategory(Category category){
        Tab categoryTab = null;
        try {
            URL location = getClass().getResource("page_view.fxml");
            fxmlLoader.setLocation(location);
            Node content = (Node) fxmlLoader.load(getClass().getResourceAsStream("page_view.fxml"));
            TabPageController tabController = (TabPageController) fxmlLoader.getController();
            //tabController.initListMeals(category);
            tabController.initViewPage(category);
        
            categoryTab = new Tab(category.getName());
            categoryTab.setContent(content); 
            
        } catch (IOException ex) {
            Logger.getLogger(MainMenuCotroller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return categoryTab;
    }
    
    public void viewMenu(){
        onOrderUpdated();// just setting total
        tabPaneMenu.getTabs().clear();
        
        for (Category category : Menu.getInstance()){
            Tab categoryTab = buildTabCategory(category);
            tabPaneMenu.getTabs().add(categoryTab);
        } 
        
        for (String comboMealCatName : Menu.getInstance().comboMealList.categories()){
            Tab comboTab = buildComboTab(comboMealCatName);
            tabPaneMenu.getTabs().add(comboTab);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        order = JavaFXMenu.getInstance().getOrder();
        order.addListener(this);
        viewMenu();
    }    
    
    
    // event handlers:
    
    public void onToOrder(ActionEvent event){
        JavaFXMenu.getInstance().goToOrder();
    }

    private Tab buildComboTab(String comboMealCatName) {
        Tab tab = new Tab(comboMealCatName);
        try {            
            Collection<ComboMeal> category = comboPool.category(comboMealCatName);
            
            URL location = getClass().getResource("combo_page.fxml");
            fxmlLoader.setLocation(location);
            Node content = (Node) fxmlLoader.load(getClass().getResourceAsStream("combo_page.fxml"));
            ComboPageController comboPageController = (ComboPageController) fxmlLoader.getController();
            
            comboPageController.initWithCombo(category);
            tab.setContent(content);
            
            
        } catch (IOException ex) {
            Logger.getLogger(MainMenuCotroller.class.getName()).log(Level.SEVERE, null, ex);
        } finally{
            return tab;
        }
    }

    @Override
    public void onOrderUpdated() {
        String summ = String.format("$ %.2f", order.total());
        labelPrice.setText(summ);
    }
}
