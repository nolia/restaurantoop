/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import javafx.event.ActionEvent;

/**
 *
 * @author Admin
 */
public interface MealOrderControlls {

    void onDelete(ActionEvent event);

    void onMinusMeal(ActionEvent event);

    void onPlusMeal(ActionEvent event);

    void update();
    
}
