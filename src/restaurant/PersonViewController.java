/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import menu.Meal;
import menu.Order;
import menu.Person;

/**
 *
 * @author Admin
 */
public class PersonViewController implements Initializable{
   @FXML Label labelNumber;
   @FXML HBox hboxMeals;

   private Person person;
   
   public void initWithPerson(Person p){
       this.person = p;
       labelNumber.setText(String.format("Person %d", p.getNumber()));
       for (Meal m : p.getMealsSet()){
           hboxMeals
            .getChildren()
            .add(ViewLoader.loadNewLittleMealNode(m, p, hboxMeals));
       }
       
       hboxMeals.setOnDragOver(new DragOverHandler(hboxMeals));
       hboxMeals.setOnDragDropped(new DragDroppedHandler());
   }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        
    }
    
   
   public class DragOverHandler implements EventHandler<DragEvent> {

        private Node target;

        public DragOverHandler(Node target) {
            this.target = target;
        }
       
        
        @Override
        public void handle(DragEvent event) {
            /* data is dragged over the target */
                System.out.println("onDragOver");
                
                /* accept it only if it is  not dragged from the same node 
                 * and if it has a string data */
                // and transe meal is not null
                if (event.getGestureSource() != target 
                    && event.getDragboard().hasString()
                    && JavaFXMenu.getInstance().getOrder().transMeal != null) {
                    /* allow for both copying and moving, whatever user chooses */
                    event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                }
                
                event.consume();
        }
       
   }
    
   public class DragDroppedHandler implements EventHandler<DragEvent>{

        @Override
        public void handle(DragEvent event) {
             /* data dropped */
                System.out.println("onDragDropped");
                // checking trans meal...
                Order order = JavaFXMenu.getInstance().getOrder();
                boolean success = false;
                Meal m = order.transMeal;
                if (m != null){
                    success = true;                    
                    boolean toShow = !person.containMeal(m);
                    person.addMeal(m);
                    if (toShow){
                        hboxMeals.getChildren().add(
                            ViewLoader.loadNewLittleMealNode(m, person, hboxMeals));
                    }
                }
                
                event.setDropCompleted(success);
                event.consume();
        }
       
   }
}
