/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.net.URL;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import menu.*;

/**
 *
 * @author Admin
 */
public class TabPageController implements Initializable{

    @FXML private ListView listViewMeals;
    @FXML private GridPane gridMeals;

    private OrderManager orderManager;
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        
    }

    void initListMeals(Category category) {
        for (Page page : category){
            for (Meal meal : page){
                listViewMeals.getItems().add(meal);
            }
        }
        
    }

    void viewPage() {
        Page page = currentPage;
        if (page == null) return;
        gridMeals.getChildren().clear();
        Iterator<Meal> pageMealIterator = page.iterator();
        for (int i = 0 ;
             i < menu.RestaurantMenu.PAGE_SIZE && pageMealIterator.hasNext();
             ++i){
            Meal meal = pageMealIterator.next();
            Node mealNode = loadViewMeal(meal);
            int col, row;
            col = i / 3;
            row = i % 3;
            gridMeals.add(mealNode, col, row);
            
        }
    }
    
    private Node loadViewMeal(Meal meal){
        Node content = null;
        try{
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("meal_view.fxml"));
            content = (Node) fxmlLoader.load(getClass().getResourceAsStream("meal_view.fxml"));
            MealController mealController = (MealController) fxmlLoader.getController();
            mealController.initWithMeal(meal);
            //mealController.setOrderManager(orderManager);
            
        }catch (Exception e){
            Logger.getLogger(MainMenuCotroller.class.getName()).log(Level.SEVERE, null, e);
        } 
        finally{
            return content;
        }           
        
    }
    
    private Page currentPage;
    private Category tabCategory;
    private ListIterator<Page> pageIterator;
    
    void initViewPage(Category category){
        tabCategory = category;
        pageIterator = tabCategory.listIterator();
        if (pageIterator.hasNext()){
            currentPage = pageIterator.next();
        }
        viewPage();
        
    }
    
    public void goNextPage(MouseEvent event){
        if (pageIterator.hasNext()){
            currentPage = pageIterator.next();
            viewPage();
        } 
    }
    
    public void goPrevPage(MouseEvent event){
        if (pageIterator.hasPrevious()){
            currentPage = pageIterator.previous();
            viewPage();
        }
    }
    
    public void onKeyPressed(KeyEvent event){
        KeyCode keyCode = event.getCode();
        if (KeyCode.RIGHT.equals(keyCode)){
            goNextPage(null);
        } else if (KeyCode.LEFT.equals(keyCode)){
            goPrevPage(null);
        }
        
    }

   
}
