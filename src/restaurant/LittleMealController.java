/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import menu.Meal;
import menu.Order;
import menu.Person;

/**
 *
 * @author Admin
 */
public class LittleMealController extends AbstractMealController implements MealOrderControlls{

    @FXML
    Button btnPlusMeal;
    @FXML
    Button btnNumber;
    @FXML
    Button btnMinus;
    @FXML
    Button btnDelete;
    
    @FXML
    Label labelMealTitle;
    @FXML
    AnchorPane anchorRoot;
    @FXML
    Rectangle rectImage;
    
    private Order order;
    private Meal theMeal;
    private Person owner;
    private ImageView theImageView;
    
    public void setPerson(Person p){
        this.owner = p;
    }
    
    @Override
    public void initWithMeal(Meal meal) {
        theMeal = meal;
        labelMealTitle.setText(meal.getName());
        theImageView = setMealImage(meal, rectImage, anchorRoot);
        order = JavaFXMenu.getInstance().getOrder();
        update();
    }

    @Override
    public void onPlusMeal(ActionEvent event) {
        owner.addMeal(theMeal);
        update();
    }
    
    @Override
    public void onMinusMeal(ActionEvent event){
        owner.reduceMealNumber(theMeal);
        update();
    }
    @Override
    public void onDelete(ActionEvent event){
        owner.deleteMealAll(theMeal);
        update();
    }

    @Override
    public void update() {
        int q = owner.mealQuantity(theMeal);
        btnNumber.setText(String.valueOf(q));
        // TODO Update parent control
        if (q == 0){
            parent.getChildren().remove(anchorRoot);
        }
        order.update();
    }

    private Pane parent;
    
    public void init(Person owner, Pane parent) {
        this.owner = owner;
        this.parent = parent;
    }
    
    private class DragDetectHandler implements EventHandler<MouseEvent>{

        private Node source;

        public DragDetectHandler(Node node) {
            this.source = node;
        }
        
        
        @Override
        public void handle(MouseEvent event) {
             //drag was detected, start drag-and-drop gesture
            System.out.println("onDragDetected");
            Dragboard db = source.startDragAndDrop(TransferMode.ANY);
            // put a string on dragboard
            ClipboardContent content = new ClipboardContent();
            content.putString(theMeal.toString());
            db.setContent(content);
            
            order.transMeal = theMeal;
            event.consume();
        }
        
    }
    
    private class DragDoneHandler implements EventHandler<DragEvent>{

        @Override
        public void handle(DragEvent event) {
            if (TransferMode.MOVE.equals(event.getTransferMode())){
                onMinusMeal(null);
                // or if target is a recycle - delete
            }
            order.transMeal = null;
            event.consume();
        }
        
    }
    
    
    public void initDragAndDrop(){
        anchorRoot.setOnDragDetected(new DragDetectHandler(anchorRoot));
        theImageView.setOnDragDetected(new DragDetectHandler(theImageView));
        final DragDoneHandler dragDoneHandler = new DragDoneHandler();
        anchorRoot.setOnDragDone(dragDoneHandler);
        theImageView.setOnDragDone(dragDoneHandler);
    }
    
}
