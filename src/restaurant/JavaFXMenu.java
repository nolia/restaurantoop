package restaurant;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import menu.*;
import menu.util.Parser;
/**
 *
 * @author rain
 */
public class JavaFXMenu extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
        
    }
    private static JavaFXMenu instance;
    private Order order = new Order();
    
    public JavaFXMenu(){
        instance = this;
    }    
    public static JavaFXMenu getInstance() {
        return instance;
    }
    
    private Stage loadMainMenu(Stage stage){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("main_menu.fxml"));
            stage.setScene(new Scene(root));
        } catch (IOException ex) {
            Logger.getLogger(JavaFXMenu.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return stage;
        }
        
    }
    
    private void openMenuPage(){
        try {
            stage.setTitle("Restaurant");
            replaceSceneContent("main_menu.fxml");
        } catch (Exception ex) {
            Logger.getLogger(JavaFXMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void start(Stage primaryStage) {
        loadMenu();
        stage = primaryStage;
        openMenuPage();
        stage.show();
        
    }
    
    
    private void testParser(){
        Menu menu = Menu.getInstance();
        Parser parser = new Parser();
        final String mealLocation = "D:\\Meals.xml";
        // TODO WARNING: Potential error here
        File xmlMeals = new File(mealLocation);
        if (!xmlMeals.exists()) {
            System.out.printf("No file at location %s \n Pleas copy or create it there",
                    mealLocation);
            return;
        }
        parser.parseMenu(xmlMeals, menu);       
        
        System.out.printf("Menu toStr: \n%s", menu);
        System.out.printf("Menu out: %s" , outPutMenu(menu));
        
        
    }

    private String outPutMenu(Menu menu) {
       Iterator<Category> menuIterator = menu.iterator();
       ListIterator<Page> categoryIterator;
 
       StringBuilder builder = new StringBuilder("Menu:\n");
       while (menuIterator.hasNext()) {
           Category currentCategory = menuIterator.next();
           categoryIterator = currentCategory.listIterator();
           builder.append("Category: ")
                  .append(currentCategory) 
                  .append('\n');
           while (categoryIterator.hasNext()) {

               Page page = categoryIterator.next();
               builder.append("page : ").append(
                       // getting Page.toString here
                       page)
                       .append('\n')
                       ;
                 // getting inside of the page:
               Iterator<Meal> pageMealsIterator = page.iterator();
               while (pageMealsIterator.hasNext()) {
                   Meal meal = pageMealsIterator.next();
                   builder.append("meal : ").append(meal).append('\n');
               }
           }
       }
       
       for (ComboMeal combo : menu.comboMealList){
           builder.append("combo: ").append(combo.asMeal()).append('\n');
       }
       for (ExtraInfo extra : menu.extraInfoList){
           builder.append("extra: ").append(extra).append('\n');
       }
     
       return builder.toString();
    }
    
    private Stage stage;
    
    private Parent replaceSceneContent(String fxml) throws Exception {
        Parent page = (Parent) FXMLLoader.load(getClass().getResource(fxml), null, new JavaFXBuilderFactory());
        Scene scene = stage.getScene();
        if (scene == null) {
            scene = new Scene(page);
            scene.getStylesheets().add(getClass().getResource("menu.css").toExternalForm());
            //scene.getStylesheets().add(this.getClass().getResource("demo.css").toExternalForm());
            stage.setScene(scene);
        } else {
            stage.getScene().setRoot(page);
        }
        stage.sizeToScene();
        return page;
    }

    private void loadMenu() {
        Menu menu = Menu.getInstance();
        Parser parser = new Parser();
        final String mealLocation = "D:\\Meals.xml";
        // TODO WARNING: Potential error here
//        File xmlMeals = new File(mealLocation);
//        if (!xmlMeals.exists()){
//            System.out.printf("No file at location %s \n Pleas copy or create it there",
//                    mealLocation  
//                    );
//            return;
//        } 
        menu = parser.parseMenuFromResurce();
    }

    public Order getOrder(){
        return this.order;
    }
    
    public void goToOrder() {
        try {
            replaceSceneContent("order_view.fxml");
        } catch (Exception ex) {
            Logger.getLogger(JavaFXMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void goToMenu() {
        try{
            replaceSceneContent("main_menu.fxml");
        }catch (Exception ex){
            Logger.getLogger(JavaFXMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
