package menu;

import java.util.*;

/**
 *
 * @author rain
 */
public class ComboMealPool extends Pool<ComboMeal> implements Iterable<ComboMeal> {
    private static ComboMealPool instance;
    
    public static ComboMealPool getInstance() {
        if (instance == null)
            instance = new ComboMealPool();
        return instance;
    }
    
    private ComboMealPool() { }
    
    public boolean isInitialized() {
        for (ComboMeal comboMeal : pool) {
            if (!comboMeal.initialized())
                return false;
        }
        return true;
    }
    
    /**
     * 
     * @param mealsList
     * @return Initialization status.
     */
    public boolean initialize(Collection<Meal> mealsList) {
        for (ComboMeal comboMeal : pool) {
            for (Meal meal : mealsList)
                if (comboMeal.inList(meal))
                    comboMeal.addMeal(meal);
        }
        return isInitialized();
    }

    @Override
    public Iterator<ComboMeal> iterator() {
        return new Iterator<ComboMeal>() {
            private Iterator<ComboMeal> iter = pool.iterator();
            
            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public ComboMeal next() {
                if (hasNext())
                    return iter.next();
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }
    
    /**
     * Order is predefined.
     * @return List of names of categories combos belong to.
     */
    public Set<String> categories() {
        Set<String> result = new LinkedHashSet<String>();
        for (ComboMeal combo : pool)
            if (!result.contains(combo.getCategoryName()))
                result.add(combo.getCategoryName());
        return result.isEmpty() ? null : result;
    }
    
    public Collection<ComboMeal> category(String name) {
        Collection<ComboMeal> result = new LinkedList<ComboMeal>();
        for (ComboMeal combo : pool)
            if (combo.getCategoryName().equals(name))
                result.add(combo);
        return result.isEmpty() ? null : result;
    }
            
    public static void main(String[] args) {
        ComboMealPool myPool = new ComboMealPool();
        myPool.add(new ComboMeal("Lunch", 12.50, "Lunches"));
        myPool.add(new ComboMeal("Spring lunch", 7.99, "Lunches"));
        System.out.println(myPool.size());
        
        Menu menu = Menu.getInstance();
        Category category = new Category("Meals");
        category.addMeal(new MenuMeal("Cheese", 3.9, null));
        category.addMeal(new MenuMeal("Bread", 1.2, null));
        category.addMeal(new MenuMeal("Soup", 3.0, null));
        category.addMeal(new MenuMeal("Vegetables", 3.0, null));
        menu.addCategory(category);
                
        //ComboMealPool with empty combos is initialized.
        System.out.println(myPool.isInitialized());
        
        ComboMeal combo = new ComboMeal("Simple", 5.0, "Lunches");
        combo.addMealName("Bread");
        combo.addMealName("Cheese");
        myPool.add(combo);
        combo = new ComboMeal("Fresh", 12.0, "Lunches");
        combo.addMealName("Soup");
        combo.addMealName("Vegetables");
        myPool.add(combo);
        
        System.out.println("Befor: " + myPool.isInitialized());
        myPool.initialize(menu.allMeals());
        System.out.println("Initialized: " + myPool.isInitialized());
        
        for (ComboMeal comboMeal : myPool) {
            System.out.println(comboMeal.new MealAdapter());
        }
        if (myPool.category("Sea") == null)
            System.out.println("OK");
        System.out.println(myPool.categories());
        for (ComboMeal comboMeal : myPool.category("Lunches"))
            System.out.println(comboMeal.new MealAdapter());
    }
}
