package menu;

/**
 *
 * @author rain
 */
public abstract class AbstractMeal implements Meal {

    @Override
    public String toString() {
        /*
         * Do not confuse when third parameter will be "null" through
         * the output.
         */
        return getName() + " : " + getPrice() + ", "
                + getType();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Meal))
            return false;
        Meal that = (Meal) obj;
        return getName().equals(that.getName())
                && getPrice() == that.getPrice()
                && getType().equals(that.getType());
    }
}
