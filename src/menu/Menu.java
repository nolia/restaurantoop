package menu;

import java.util.*;

/**
 *
 * @author rain
 */
public class Menu implements Iterable<Category> {

    private static Menu menu;

    public static Menu getInstance() {
        if (menu == null) {
            menu = new Menu();
        }
        return menu;
    }
    
    private Set<Category> categories = new LinkedHashSet<Category>();
    
    public ExtraInfoPool extraInfoList = ExtraInfoPool.getInstance();
    public ComboMealPool comboMealList = ComboMealPool.getInstance();
    
    private Menu() { }
    
    public void addCategory(Category category) {
        categories.add(category);
    }

    public Category getCategory(String name) {
        name = name.toLowerCase();
        Iterator<Category> iter = iterator();
        while (iter.hasNext()) {
            Category category = iter.next();
            if (category.getName().toLowerCase().equals(name)) {
                return category;
            }
        }
        return null;
    }

    int getMealsNumber() {
        int result = 0;
        for (Category category : categories)
            for (Page page : category)
                result += page.getMealsNumber();
        return result;
    }

    @Override
    public Iterator<Category> iterator() {
        return new Iterator<Category>() {
            private Iterator<Category> iter = categories.iterator();
            
            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public Category next() {
                if (hasNext())
                    return iter.next();
                throw  new NoSuchElementException();
                }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }

    /**
     *
     * @return Collection that contains all meals from the Menu.
     */
    public Collection<Meal> allMeals() {
        Collection<Meal> result = new ArrayList<Meal>(getMealsNumber());
        for (Category category : categories)
            for (Page page : category)
                for (Meal meal : page)
                    result.add(meal);
        return result;
    }

    public static void main(String[] args) {
        Menu myMenu = Menu.getInstance();

        Category category = new Category("Potato");

        Meal meal1 = new MenuMeal("Fried potato", 5.50, "Fried potato with garlic.");
        ((MenuMeal)meal1).setType(MealType.SECOND);
        category.addMeal(meal1);

        Meal meal2 = new MenuMeal("Boiled potato", 4.25, "Boiled potato with butter");
        ((MenuMeal)meal2).setType(MealType.SECOND);
        ExtraInfo spice = new ExtraInfo("Pepper");
        spice.addLevel(0.05, "Little pepper");
        spice.addLevel(0.10, "Mead pepper");
        spice.setSuitableMeals(Arrays.asList("Boiled potato", "Meat"));
        System.out.println("Suitable to meal1: " + spice.suitableTo(meal1));
        System.out.println("Suitable to meal2: " + spice.suitableTo(meal2));
        meal2 = new Extra(meal2, spice, 0);
        category.addMeal(meal2);

        System.out.println("Potato:");
        Page page = category.listIterator().next();
        Iterator<Meal> iter = page.iterator();
        while (iter.hasNext())
            System.out.println(iter.next());

        myMenu.addCategory(category);

        category = new Category("Wine");
        category.addMeal(new MenuMeal("777 port", 14.80, "Vino."));

        myMenu.addCategory(category);
        System.out.println("pages in potato: " + category.size());

        System.out.println("Menu:");
        Iterator<Category> menuIterator = myMenu.iterator();
        while (menuIterator.hasNext()) {
            ListIterator<Page> categoryIterator = menuIterator.next().listIterator();
            while (categoryIterator.hasNext()) {
                System.out.println("categoryIterator.nextIndex()=" + categoryIterator.nextIndex());
                System.out.println("categoryIterator.previousIndex()=" + categoryIterator.previousIndex());
                Iterator<Meal> pageIterator = categoryIterator.next().iterator();
                while (pageIterator.hasNext()) {
                    System.out.println("Menu element: " + pageIterator.next());
                }
            }
            while (categoryIterator.hasPrevious()) {
                System.out.println("categoryIterator.nextIndex()=" + categoryIterator.nextIndex());
                System.out.println("categoryIterator.previousIndex()=" + categoryIterator.previousIndex());
                Iterator<Meal> pageIterator = categoryIterator.previous().iterator();
                while (pageIterator.hasNext()) {
                    System.out.println("Menu element: " + pageIterator.next());
                }
            }
        }

        System.out.println(myMenu.getCategory("potato"));

        System.out.println("Menu size: " + myMenu.getMealsNumber());
        for (Meal myMeal : myMenu.allMeals()) {
            System.out.println(myMeal);
        }
    }

    @Override
    public String toString() {
        return "Menu{" + "categories=" + categories + ", extraInfoList=" + extraInfoList + ", comboMealList=" + comboMealList + '}';
    }
    /** Calls {@link ComboMealPool#initialize(java.util.Collection) }
     * with {@link #allMeals() } as a parameter
     * 
     */
    public void initializeComboMeals() {
        this.comboMealList.initialize(allMeals());
    }
}
