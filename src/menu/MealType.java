package menu;

/**
 * Not categories but predefined general types of meals.
 * @author rain
 */
public enum MealType {
    FIRST, SECOND, SALAD, DESSERT, DRINK, COMBO;
    
    public static void main(String[] args) {
        System.out.println(FIRST);
        System.out.println(DRINK);
    }
}
