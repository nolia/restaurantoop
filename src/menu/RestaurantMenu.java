package menu;

/**
 * Contains constants for application.
 * @author rain
 * 
 */
public interface RestaurantMenu {
    int PAGE_SIZE = 6;
    int MAX_GUESTS_NUMBER = 6;
}
