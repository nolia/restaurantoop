package menu;

import java.util.NoSuchElementException;

/**
 *
 * @author rain
 */
public class LackedGuestException extends NoSuchElementException {

    /**
     * Creates a new instance of
     * <code>LackedGuestException</code> without detail message.
     */
    public LackedGuestException() {
    }

    /**
     * Constructs an instance of
     * <code>LackedGuestException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public LackedGuestException(String msg) {
        super(msg);
    }
}
