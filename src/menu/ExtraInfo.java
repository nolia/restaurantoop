package menu;

import java.util.*;

/**
 *
 * @author rain
 */
public class ExtraInfo implements Iterable<ExtraInfo.Level> {
    private String name;
    private double price = 0;   //Default price. Do not changed for "groups".
    
    private List<String> mealsFor;

    public void setPrice(double price) {
        this.price = price;
    }
    
    public static class Level {
        double price;
        String descript;
        
        Level(double price, String descript) {
            this.price = price;
            this.descript = descript;
        }
    }
    private List<Level> levels;
    
    public ExtraInfo(String name) {
        this.name = name;
    }
    public ExtraInfo(String name, double price) {
        this.name = name;
        this.price = price;
    }
      
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setSuitableMeals(Collection<String> suitableMeals) {
        mealsFor = new ArrayList<String>(suitableMeals);
    }
    
    public boolean suitableTo(Meal meal) {
        if (mealsFor == null)
            throw  new IllegalStateException("Suitable meals are not specified.");
        return mealsFor.contains(meal.getName());
    }
    
    public boolean isVariable() {
        return getLevelsNumber() > 0;
    }

    /*
     * Methods about "group extra".
     * Each of levels is value of another variant of extra selection.
     */
    public int getLevelsNumber() {
        if (levels == null)
            return 0;
        return levels.size();
    }
    
    public Level getLevel(int number) {
        if (number < getLevelsNumber())
            return levels.get(number);
        if (isVariable())
            throw new LackedLevelException("Level's number out of range.");
        else
            throw  new LackedLevelException("ExtraInfo contain no levels.");
    }

    /*
     * Maybe, implementation of Iterable unneeded.
     */
    @Override
    public Iterator<Level> iterator() {
        return new Iterator<Level>() {
            private int index = 0;
            
            @Override
            public boolean hasNext() {
                return index < getLevelsNumber();
            }

            @Override
            public Level next() {
                try {
                    return getLevel(index++);
                } catch (LackedLevelException ex) {
                    throw new NoSuchElementException();
                }
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }
    
    public void addLevel(Level level) {
        if (levels == null)
            levels = new ArrayList<Level>();
        levels.add(level);
    }
    
    public void addLevel(double  price, String descript) {
        addLevel(new Level(price, descript));
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("ExtraInfo: "
                + name + " for");
        for (String mealName : mealsFor)
            result.append(' ').append(mealName).append(",");
        result.setCharAt(result.length() - 1, '.');
        return result.toString();
    }
}