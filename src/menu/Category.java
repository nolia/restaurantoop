package menu;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * 
 * @author rain
 */
public class Category implements Iterable<Page> {
    //TODO : Pool
    private String name;
    
    private LinkedList<Page> pages = new LinkedList<Page>();
    
    //  Because we do not add directly pages, just meals.
    {
        pages.add(new Page());
    }
    
    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
          
    public void addMeal(Meal meal) {
        if (pages.getLast().isFull())
            pages.add(new Page());
        pages.getLast().addMeal(meal);
    }
    
    public int size() {
        return pages.size();
    }
    
    public ListIterator<Page> listIterator() {
        return new ListIterator<Page>() {
            private int index = 0;
            
            @Override
            public boolean hasNext() {
                return index < pages.size();
            }

            @Override
            public Page next() {
                if (hasNext())
                    return pages.get(index++);
                throw new NoSuchElementException("No next.");
            }

            @Override
            public boolean hasPrevious() {
                return index > 0;
            }

            @Override
            public Page previous() {
                if (hasPrevious())
                    return pages.get(--index);
                throw new NoSuchElementException("No previous.");
            }

            @Override
            public int nextIndex() {
                return index;
            }

            @Override
            public int previousIndex() {
                return index - 1;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }

            @Override
            public void set(Page e) {
                throw new UnsupportedOperationException("Not supported.");
            }

            @Override
            public void add(Page e) {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }

    @Override
    public Iterator<Page> iterator() {
        return listIterator();
    }

    @Override
    public String toString() {
        return "Category \"" + name + "\", " + size() + " pages.";
    }

    public void addComboMeal(ComboMeal currentCombo) {
        
        
    }
    
}
