package menu;

/**
 *
 * @author rain
 */
public class Recommendation {
    private String mealName;
    private String descript;
    
    public Recommendation(String mealName, String descript) {
        this.mealName = mealName;
        this.descript = descript;
    }

    public String getDescription() {
        return descript;
    }

    public String getMealName() {
        return mealName;
    }
    
    public boolean suitFor(AbstractMeal meal) {
        return mealName.toLowerCase().
                equals(meal.getName().toLowerCase());
    }

    @Override
    public String toString() {
        return "Recommendation for " + mealName;
    }
}
