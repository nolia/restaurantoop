package menu;

/**
 *
 * @author rain
 */
public interface Meal {

    String getDescription();

    String getImageName();

    String getName();

    double getPrice();

    MealType getType();

    /**
     *
     * @return Description of recommendation.
     */
    String recommendedWith(Meal meal);
    
}
