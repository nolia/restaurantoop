package menu;

import java.util.NoSuchElementException;

/**
 * 
 * @author rain
 */
public class LackedLevelException extends NoSuchElementException {

    /**
     * Creates a new instance of
     * <code>LackedLevelException</code> without detail message.
     */
    public LackedLevelException() {
    }

    /**
     * Constructs an instance of
     * <code>LackedLevelException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public LackedLevelException(String msg) {
        super(msg);
    }
}
