/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

/**
 *
 * @author rain
 */
public class PageOverflowError extends Error {

    /**
     * Creates a new instance of
     * <code>PageOverflowError</code> without detail message.
     */
    public PageOverflowError() {
    }

    /**
     * Constructs an instance of
     * <code>PageOverflowError</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public PageOverflowError(String msg) {
        super(msg);
    }
}
