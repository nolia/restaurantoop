package menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author rain
 */
public class ExtraInfoPool extends Pool<ExtraInfo> implements Iterable<ExtraInfo> {
    private static ExtraInfoPool instance;
    
    public static ExtraInfoPool getInstance() {
        if (instance == null)
            instance = new ExtraInfoPool();
        return instance;
    }
    
    private ExtraInfoPool() { }
    
    public Collection<ExtraInfo> getSuitable(Meal meal) {
        if (pool == null)
            return null;
        ArrayList<ExtraInfo> result = new ArrayList<ExtraInfo>();
        Iterator<ExtraInfo> iter = pool.iterator();
        while (iter.hasNext()) {
            ExtraInfo extraInfo = iter.next();
            if (extraInfo.suitableTo(meal))
                result.add(extraInfo);
        }
        if (!result.isEmpty())
            return result;
        return null;
    }
    
    public static void main(String[] args) {
        ExtraInfoPool pool  = ExtraInfoPool.getInstance();
        
        ExtraInfo info = new ExtraInfo("Salt", 0.03);
        info.setSuitableMeals(Arrays.asList("potato", "soup", "Salad"));
        pool.add(info);

        info = new ExtraInfo("Tomato");
        info.addLevel(new ExtraInfo.Level(2.0, "Red tomatoes"));
        info.setSuitableMeals(Arrays.asList("soup"));
        pool.add(info);
        
        System.out.println(pool.size());
        
        Collection<ExtraInfo> meals = pool.getSuitable(new MenuMeal("Salad", 3.99,
                "Cabbage salad"));
        System.out.println(meals.size());
        
        meals = pool.getSuitable(new MenuMeal("soup", 5.00, null));
        Iterator<ExtraInfo> iter = meals.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }
    }

    @Override
    public Iterator<ExtraInfo> iterator() {
        return pool.iterator();
    }
}
