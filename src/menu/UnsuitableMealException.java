/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

/**
 *
 * @author rain
 */
public class UnsuitableMealException extends RuntimeException {

    /**
     * Creates a new instance of
     * <code>UnsuitableMealException</code> without detail message.
     */
    public UnsuitableMealException() {
    }

    /**
     * Constructs an instance of
     * <code>UnsuitableMealException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public UnsuitableMealException(String msg) {
        super(msg);
    }
}
