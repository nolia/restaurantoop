package menu;

import java.util.*;

/**
 *
 * @author rain
 */
public class ComboMeal implements Iterable<Meal> {

    private String name;
    private double price;
    private String categoryName;    //Name of category where combo have to be shown.
    private String iconName;
    // N: think name <mealList> is amgbigous - renamed
    private List<String> mealNamesList;
    private Set<Meal> meals;

    public ComboMeal(String name, double price, String category) {
        this.name = name;
        this.price = price;
        this.categoryName = category;
    }

    public String getCategoryName() {
        return categoryName;
    }
    
    public void setImageName(String imageName) {
        iconName = imageName;
    }

    public void addMealName(String mealName) {
        if (mealNamesList == null) {
            mealNamesList = new ArrayList<String>();
        }
        mealNamesList.add(mealName);
    }

    public boolean inList(Meal meal) {
        if (mealNamesList == null) {
            return false;
        }
        return mealNamesList.contains(meal.getName());
    }

    public boolean addMeal(Meal meal) {
        if (!inList(meal)) {
            return false;
        }

        if (meals == null) {
            meals = new HashSet<Meal>();
        }
        meals.add(meal);
        return true;
    }

    /**
     *
     * @param mealsList
     * @return Initialization status.
     */
    public boolean initialize(Collection<Meal> mealsList) {
        for (Meal meal : mealsList) {
            if (inList(meal)) {
                meals.add(meal);
            }
        }
        return initialized();
    }

    public boolean contains(Meal meal) {
        if (meals == null) {
            return false;
        }
        return meals.contains(meal);
    }

    public int size() {
        if (mealNamesList == null) {
            return 0;
        }
        return mealNamesList.size();
    }

    int contentSize() {
        return meals == null ? 0 : meals.size();
    }

    public boolean initialized() {
        return size() == contentSize();
    }

    @Override
    public Iterator<Meal> iterator() {
        return new Iterator<Meal>() {

            Iterator<Meal> iter;

            {
                if (meals != null) {
                    iter = meals.iterator();
                }
            }

            @Override
            public boolean hasNext() {
                if (iter == null) {
                    return false;
                }
                return iter.hasNext();
            }

            @Override
            public Meal next() {
                if (hasNext()) {
                    return iter.next();
                }
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }
    
    public Meal asMeal() {
        return this.new MealAdapter();
    }

    public String getName() {
        return this.name;
    }

    public String getPrice() {
        return String.valueOf(this.price);
    }

    public class MealAdapter extends AbstractMeal {

        @Override
        public String getName() {
            return name;
        }

        @Override
        public double getPrice() {
            return price;
        }

        @Override
        public String getDescription() {
            //Combo have no common description (and such a button).
            return "NO DESCRIPTION";
        }

        @Override
        public MealType getType() {
            return MealType.COMBO;
        }

        @Override
        public String recommendedWith(Meal meal) {
            //Combo can't have recommendation for it.
            //All recommendations in box (:
            return null;
        }

        @Override
        public String getImageName() {
            return iconName;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder(getName()
                    + " : costs " + getPrice());
            if (mealNamesList != null) {
                result.append(", consists of");
                for (String mealName : mealNamesList) {
                    result.append(' ').append(mealName).append(',');
                }
            } else {
                result.append(" is empty.");
            }
            result.setCharAt(result.length() - 1, '.');
            return result.toString();
        }
    }

    public static void main(String[] args) {
        ComboMeal combo = new ComboMeal("Sea lunch", 61.50, "Lunches");
        combo.setImageName("sea.jpg");
        combo.addMealName("Sea soup");
        combo.addMealName("Fish");
        combo.addMealName("Shrimps");
        System.out.println(combo.inList(new MenuMeal("Fish", 3.90, "Flat fish.")));
        System.out.println(combo.contains(new MenuMeal("Fish", 3.90, "Flat fish.")));
        System.out.println("Size : " + combo.size());

        Meal meal1 = new MenuMeal("Fish", 9.0, "Sea fish");
        Meal meal2 = new MenuMeal("Shrimps", 19.0, "Delicate shrimps.");
        combo.addMeal(meal1);
        System.out.println(combo.addMeal(meal2));
        System.out.println(combo.addMeal(new MenuMeal("River fish", 30.90, null)));
        System.out.println(combo.contains(meal1));
        System.out.println(combo.contentSize());
        System.out.println(combo.initialized());

        for (Iterator<Meal> iter = combo.iterator(); iter.hasNext();) {
            System.out.println(iter.next());
        }

        MealAdapter aMeal = combo.new MealAdapter();
        System.out.println(aMeal);
        System.out.println(aMeal.getType());
        System.out.println(aMeal.getDescription());
        System.out.println(aMeal.getImageName());
        System.out.println(aMeal.recommendedWith(new MenuMeal("anything", 50, null)));
    }
}
