package menu;

import java.util.*;

/**
 *
 * @author rain
 */
public class Person implements Iterable<Meal> {

    private int number;

    public int getNumber() {
        return number;
    }
    private Map<Meal, Integer> meals;

    public Map<Meal, Integer> getMeals() {
        return meals;
    }

    public Set<Meal> getMealsSet() {
        return meals.keySet();
    }

    public Person(int id) {
        number = id;
        meals = new LinkedHashMap<Meal, Integer>();
    }

    /**
     *
     * @param meal
     * @return Quantity of added meal in this person.
     */
    public int addMeal(Meal meal) {
        if (meals == null) {
            meals = new LinkedHashMap<Meal, Integer>();
        }
        meals.put(meal, meals.get(meal) == null ? 1 : meals.get(meal) + 1);
        return mealQuantity(meal);
    }

    /**
     *
     * @param meal
     * @return New number of the meal.
     */
    public int reduceMealNumber(Meal meal) {
        if (containMeal(meal)) {
            int currentNumber = meals.remove(meal);
            if (currentNumber > 1) {
                meals.put(meal, currentNumber - 1);
            }
        }
        return mealQuantity(meal);
    }

    public boolean containMeal(Meal meal) {
        return meals != null && meals.containsKey(meal);
    }

    public int mealQuantity(Meal meal) {
        Integer result = meals.get(meal);
        return result == null ? 0 : result;
    }

    public int numberOfMeals() {
        return meals == null ? 0 : meals.size();
    }

    /**
     *
     * @deprecated
     */
    public void setMealNumber(Meal meal, int quantity) {
        if (meals == null) {
            meals = new LinkedHashMap<Meal, Integer>();
        }
        meals.put(meal, quantity);
    }

    /**
     *
     * @param meal
     * @return Number of remains meal.
     */
    public int takeAway(Meal meal) {
        if (containMeal(meal)) {
            meals.remove(meal);
            return numberOfMeals();
        }
        throw new NoSuchElementException();
    }

    @Override
    public Iterator<Meal> iterator() {
        return new Iterator<Meal>() {

            Set<Map.Entry<Meal, Integer>> mapSet = meals.entrySet();
            Iterator<Map.Entry<Meal, Integer>> iter = mapSet.iterator();

            @Override
            public boolean hasNext() {
                return iter.hasNext();
            }

            @Override
            public Meal next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                Map.Entry<Meal, Integer> nextEntry = iter.next();
                return nextEntry.getKey();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }

    @Override
    public String toString() {
        return "Person: " + "id=" + number + ", contains " + numberOfMeals()
                + " meals.";
    }

    public void deleteMealAll(Meal theMeal) {
         meals.remove(theMeal);
    }
}
