package menu;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Extra in the form of decorator for meal.
 * @author rain
 * 
 */
public class Extra extends AbstractMeal {
    private Meal meal;
    
    private ExtraInfo info;
    private int selected;
    
    public Extra(Meal meal, ExtraInfo info) {
        if (!info.suitableTo(meal))
            throw  new UnsuitableMealException("meal and extra"
                    + "are incompatible.");
        this.meal = meal;
        this.info = info;
    }
    
    public Extra(Meal meal, ExtraInfo info, int selected) {
        this(meal, info);
        if (info.getLevelsNumber() <= selected)
            throw new LackedLevelException("Selected illegal.");
        this.selected = selected;
    }
    
    @Override
    public String getName() {
        return meal.getName() + "+";
    }

    @Override
    public double getPrice() {
        double price =  meal.getPrice() + info.getPrice();
        if (info.isVariable()) {
            try {
                price += info.getLevel(selected).price;
            } catch (LackedLevelException ex) {
                throw new Error("IsVariable == true", ex);
            }
        }
        return price;
    }
    
    @Override
    public String getDescription() {
        String descript = meal.getDescription() + " " + info.getName();
        if (info.isVariable()) {
            try {
                descript += ", " + info.getLevel(selected).descript;
            } catch (LackedLevelException ex) {
                throw new Error("IsVarible() == true", ex);
            }
        }
        return descript;
    }

    @Override
    public MealType getType() {
        return meal.getType();
    }

    @Override
    public String recommendedWith(Meal meal) {
        return this.meal.recommendedWith(meal);
    }

    @Override
    public String getImageName() {
        return meal.getImageName();
    }
    
    public static void main(String[] args) {
        ExtraInfo tomato = new ExtraInfo("Tomato");
        tomato.setSuitableMeals(Arrays.asList("Mc Cheeken", "Pizza"));
        tomato.addLevel(3.22, "Single");
        tomato.addLevel(1.2, "Double");
        
        Iterator<ExtraInfo.Level> iter = tomato.iterator();
        while (iter.hasNext())
            System.out.println(iter.next().descript);
        
        MenuMeal item = new MenuMeal("Mc Cheeken", 13.99, "Very good tasting!");
        item.setType(MealType.SECOND);
        Meal mcChicken = item;
        try {
            mcChicken = new Extra(mcChicken, tomato, 1);
        } catch (LackedLevelException ex) {
            System.out.println("Level addition was expected.");
        } catch (UnsuitableMealException ex) {
            System.out.println("Wrong tomato.setSuitableMeals().");
        }
        System.out.println(mcChicken);
        System.out.println(mcChicken.getDescription());
        
        //Test recommendations
        MenuMeal menuMeal = new MenuMeal("Soup", 3.90, null);
        menuMeal.addRecommendation(new Recommendation("Vegatables",
                "Really good with soup"));
        Meal anotherMeal = new MenuMeal("Vegatables", 9.0, null);
        System.out.println(menuMeal.recommendedWith(anotherMeal));
        Meal meal = menuMeal;
        
        ExtraInfo info = new ExtraInfo("Bred", 0.8);
        info.setSuitableMeals(Arrays.asList("Soup"));
        meal = new Extra(menuMeal, info);
        meal.recommendedWith(anotherMeal);
        System.out.println(meal + " : " + meal.recommendedWith(anotherMeal));
        
        System.out.println(new MenuMeal("Pasta", 4.45, null)
                .recommendedWith(new MenuMeal("Cucumbers", 2.50, null)));
    }
}
