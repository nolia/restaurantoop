/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

/**
 *
 * @author Admin
 */
public class FXMLWrapper {
    private Node content;

    public Node getContent() {
        return content;
    }

    public void setContent(Node content) {
        this.content = content;
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public FXMLLoader getLoader() {
        return loader;
    }

    public void setLoader(FXMLLoader loader) {
        this.loader = loader;
    }
    private Object controller;
    private FXMLLoader loader = new FXMLLoader();
    
    
    
    public FXMLWrapper loadNewView( Class classT, String location){
        try {
            loader.setLocation(classT.getResource(location));
            content = (Node) loader.load(classT.getResourceAsStream(location));
            controller = loader.getController();
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLWrapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return this;
        }
    }
    
    
}
