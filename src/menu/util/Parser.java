package menu.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import menu.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author Admin
 */
public class Parser {
        
    private class XmlHandler extends DefaultHandler {
        
        private Menu mMenu;
        private LinkedList<String> pathStack = new LinkedList<String>();
        
        private MenuMeal currentMeal;
        private Recommendation currentRecommendation;
        
        private Category currentCategory;
        private List<String> suitableMeals = new ArrayList<String>();

        private boolean inCombo = false;
        private boolean inExtra = false;
        private boolean inMealName = false;
        
        private ComboMeal currentCombo;
        private ExtraInfo currentExtra;
        private boolean toAddCurrentCategory;
        
        public void setMenu(Menu menu) {
            this.mMenu = menu;
        }

        public Menu getMenu() {
            return mMenu;
        }
        
        
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            pathStack.push(qName);
            // asserting right order here
            if (XmlContract.CATEGORY.equals(qName)){
                toAddCurrentCategory = true;
                newCategory(attributes);
            }else if (XmlContract.MEAL.equals(qName)){
                newMeal(attributes);
            } else if (XmlContract.RECOMMENDATION.equals(qName)){
                addNewRecommendation(attributes);
            } else if (XmlContract.COMBO_MEAL.equals(qName)){
                inCombo = true; 
                newCombo(attributes);
            } else if (XmlContract.MEAL_NAME_REF.equals(qName)){
                inMealName = true;
            } else if (XmlContract.EXTRA.equals(qName)){
                inExtra = true;
                newExtra(attributes);
            } else if (XmlContract.LEVEL.equals(qName)){
                newLevelToExtra(attributes);
            }
            
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
           // will probably be unused    
            String str = new String(ch, start, length);
            if (inCombo && inMealName){
                currentCombo.addMealName(str);
            }
            if (inExtra && inMealName){
                suitableMeals.add(str);
            }
        }
        
      

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            // this code by default asserts right order of statements in XML
            if (XmlContract.RECOMMENDATION.equals(qName)){
                currentMeal.addRecommendation(currentRecommendation);
                currentRecommendation = null; // avoiding leaks, probably
            } else if (XmlContract.MEAL.equals(qName)){
                currentCategory.addMeal(currentMeal);
            } else if (XmlContract.CATEGORY.equals(qName)){
               if (toAddCurrentCategory){
                    mMenu.addCategory(currentCategory);
                    currentCategory = null;
                }
            }else if (XmlContract.COMBO_MEAL.equals(qName)){
                inCombo = false;
                mMenu.comboMealList.add(currentCombo);
                currentCombo = null;
            } else if (XmlContract.MEAL_NAME_REF.equals(qName)){
                inMealName = false;
            } else if (XmlContract.EXTRA.equals(qName)){
                inExtra = false;
                currentExtra.setSuitableMeals(suitableMeals);
                suitableMeals = new ArrayList<String>();
                mMenu.extraInfoList.add(currentExtra);
                currentExtra = null;
            } else if (XmlContract.LEVEL.equals(qName)){
                // level is added in startElement
            }
        }

        private void newCategory(Attributes attributes) {
            String name = attributes.getValue(XmlContract.NAME);
            if (name != null){
                currentCategory = new Category(name);
            } else 
                throw new Error("No <name> attr in XML Category");
        }

        private void newMeal(Attributes attributes) {
            String name = attributes.getValue(XmlContract.NAME);
            String priceStr = attributes.getValue(XmlContract.PRICE);
            if (name == null || priceStr == null){
                throw new Error("XML contract broken");
            }
            
            String descr = attributes.getValue(XmlContract.DESCRIPT); // unvaluable
            double price = Double.parseDouble(priceStr);
            currentMeal = new MenuMeal(name, price, descr);
            String imageName = attributes.getValue(XmlContract.IMAGE);
            currentMeal.setImageName(imageName);
        }

        private void addNewRecommendation(Attributes attributes) {
            String mealName = attributes.getValue(XmlContract.MEAL_NAME);
            String descr = attributes.getValue(XmlContract.DESCRIPT);
            if (mealName == null || descr == null) {
                throw new Error("XML contract broken");
            } 
            currentRecommendation = new Recommendation(mealName, descr);
        }

        private void newCombo(Attributes attributes) {
            String name = attributes.getValue(XmlContract.NAME);
            String priceStr = attributes.getValue(XmlContract.PRICE);
            if (name == null || priceStr == null){
                throw new Error("XML contract broken");
            }
            String descr = attributes.getValue(XmlContract.DESCRIPT); // unvaluable
            double price = Double.parseDouble(priceStr);
            currentCombo = new ComboMeal(name, price, currentCategory.getName());
            toAddCurrentCategory = false;
         //   currentCategory.addMeal(currentCombo.asMeal());
         //   currentCategory.addComboMeal(currentCombo);
        }

        private void newExtra(Attributes attributes) {
            String name = attributes.getValue(XmlContract.NAME);
            String priceStr = attributes.getValue(XmlContract.PRICE);
            if (name == null ){
                throw new Error("XML contract broken");
            }
            currentExtra = new ExtraInfo(name);
            if (priceStr != null){
                double price = Double.parseDouble(priceStr);
                currentExtra.setPrice(price);
            }
        }

        private void newLevelToExtra(Attributes attributes) {
            String descript = attributes.getValue(XmlContract.DESCRIPT);
            String priceStr = attributes.getValue(XmlContract.PRICE);
            if (descript == null || priceStr == null){
                throw new Error("XML contract broken");
            }
            String descr = attributes.getValue(XmlContract.DESCRIPT); // unvaluable
            double price = Double.parseDouble(priceStr);
            currentExtra.addLevel(price, descript);
        }

        
    }
    private XmlHandler mHandler;

    public Parser() {
        this.mHandler = new XmlHandler();
    }
    
    public Menu parseMenuFromResurce(){
        Menu menu = Menu.getInstance();
        mHandler.setMenu(menu);
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            InputStream stream = getClass().getResourceAsStream("Meals.xml");
            
            factory.newSAXParser().parse(stream, mHandler);
            //initing
            menu.initializeComboMeals();
        } catch (SAXException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            return menu;
        }
    }
        
    public Menu parseMenu(File xmlFile, Menu menu) {
        mHandler.setMenu(menu);
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.newSAXParser().parse(xmlFile, mHandler);
            //initing
            menu.initializeComboMeals();
        } catch (SAXException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            return menu;
        }
    }
}
