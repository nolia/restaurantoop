/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

/**
 *
 * @author rain
 */
public class GuestsNumberError extends Error {

    /**
     * Creates a new instance of
     * <code>GuestsNumberError</code> without detail message.
     */
    public GuestsNumberError() {
    }

    /**
     * Constructs an instance of
     * <code>GuestsNumberError</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public GuestsNumberError(String msg) {
        super(msg);
    }
}
