package menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * 
 * @author rain
 */
public class Page implements Iterable<Meal> {
    /*
     * Static page countment management.
     */
    private static int pageCounter = 1; //Number of next page that will be added.

    public static int getPageCounter() {
        return pageCounter;
    }

    public static void setPageCounterTo(int pageCounter) {
        Page.pageCounter = pageCounter;
    }
    
    private final int number = pageCounter++;
    private List<Meal> pageContent;
    
    public int getNumber() {
        return number;
    }
    
    void addMeal(Meal meal) {
        if (pageContent == null)
            pageContent = new ArrayList<Meal>(RestaurantMenu.PAGE_SIZE);
        if (pageContent.size() == RestaurantMenu.PAGE_SIZE)
            throw new PageOverflowError("Page size is limited to " + RestaurantMenu.PAGE_SIZE);
        pageContent.add(meal);
        }
    
    public Meal getMeal(String mealName) {
        mealName = mealName.toLowerCase();
        if (pageContent == null)
            return null;
        Iterator<Meal> iter = pageContent.iterator();
        while(iter.hasNext()) {
            Meal nextMeal = iter.next();
            if (nextMeal.getName().toLowerCase().equals(mealName))
                return nextMeal;
        }
        return null;           
    }
    
    public int getMealsNumber() {
        if (pageContent == null)
            return 0;
        return pageContent.size();
    }
    
    public boolean isFull() {
        return getMealsNumber() == RestaurantMenu.PAGE_SIZE;
    }
    
    @Override
    public Iterator<Meal> iterator() {
        return new Iterator<Meal>() {
            private int index = 0;
            
            @Override
            public boolean hasNext() {
                return pageContent == null ? false : index < pageContent.size();
            }

            @Override
            public Meal next() {
                if (hasNext())
                    return pageContent.get(index++);
                throw new NoSuchElementException();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Not supported.");
            }
        };
    }

    @Override
    public String toString() {
        return "Page " + number + ", " + getMealsNumber() + " meals.";
    }
}
