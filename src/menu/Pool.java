package menu;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @author rain
 */
public class Pool<E> {

    protected Set<E> pool;

    public void add(E element) {
        if (pool == null) {
            pool = new LinkedHashSet<E>();
        }
        pool.add(element);
    }

    public int size() {
        if (pool == null) {
            return 0;
        }
        return pool.size();
    }
    
}
