package menu;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rain
 */
public class MenuMeal extends AbstractMeal {
    private String name;
    private double price;
    private String description;
    private MealType type;
    private String imageName;
    private List<Recommendation> recommendations;

    public MenuMeal(String name, double price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setType(MealType type) {
        this.type = type;
    }

    @Override
    public String getImageName() {
        return imageName;
    }

    @Override
    public MealType getType() {
        return type;
    }

    public void addRecommendation(Recommendation r) {
        if (recommendations == null)
            recommendations = new ArrayList<Recommendation>();
        recommendations.add(r);
    }
    
    @Override
    public String recommendedWith(Meal meal) {
        if (recommendations == null)
            return null;
        for (Recommendation Recommendation : recommendations) {
            if (Recommendation.getMealName().toLowerCase().equals(meal.getName().toLowerCase()))
                return Recommendation.getDescription();
        }
        return null;
    }
    
    public  static void main(String[] args) {
        MenuMeal myMeal = new MenuMeal("Mc Cheeken", 13.99, "Very good tasting!");
        System.out.println(myMeal);
        System.out.println(myMeal.getDescription());
        
        myMeal.setType(MealType.SECOND);
        myMeal.setImageName("mc_cheeken.jpg");
        System.out.println(myMeal.getImageName());
        
        Meal meal = myMeal;
        System.out.println(meal);
        System.out.println(meal.getDescription());
        System.out.println(meal.getType());
        System.out.println(meal.getPrice());
    }
}
