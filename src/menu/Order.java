package menu;

import java.util.*;
import menu.util.OrderListener;

/**
 *
 * @author rain
 */
public class Order {
    private List<Person> guests;
    public Person common = new Person(0);

    /**
     * A meal, used as tans buffer during drag n drop
     */
    public Meal transMeal;
    
    public Order() {
        guests = new ArrayList<Person>();
    }
    
    public Order(int guestsNumber) {
        guests = new ArrayList<Person>(guestsNumber);
        for (int i = 1; i <= guestsNumber; ++i)
            guests.add(new Person(i));
    }
    
    public int getGuestsNumber() {
        return guests.size();
    }
    
    /**
     * Guests number begane from 1.
     * @param number
     * @return
     */
    public Person getGuest(int number) {
        if (number <= getGuestsNumber())
            return guests.get(number - 1);
        throw new LackedGuestException();
    }
    
    /**
     * 
     * @return New number of guests.
     */
    public int addGuest() {
        if (getGuestsNumber() == RestaurantMenu.MAX_GUESTS_NUMBER)
            throw new GuestsNumberError();
        guests.add(new Person(getGuestsNumber() + 1));
        return getGuestsNumber();
    }
    
    /**
     * 
     * @return New number of guests.
     */
    public int removeGuest() {
        if (getGuestsNumber() == 0)
            throw new GuestsNumberError("Nothing to remove");
        //TODO : Maybe, if number of guests became one, copy common meals to the first person.
        guests.remove(getGuestsNumber() - 1);
        return getGuestsNumber();
    }
    
    public void addMeal(Meal meal) {
        common.addMeal(meal);
    }
    
    int getMealsNumber() {
        int result = 0;
        for (Person person : guests)
            result += person.numberOfMeals();
        result += common.numberOfMeals();
        return result;
    }
    
    public Map<Meal, String> getRecommendations(int quantity) {
        Map<Meal, String> result = new HashMap<Meal, String>();
        
        Set<Meal> orderedMeals = new HashSet<Meal>(getMealsNumber());
        for (Person person : guests)
            for (Meal meal : person)
                orderedMeals.add(meal);
        for (Meal meal : common)
            orderedMeals.add(meal);
        
        Collection<Meal> menuMeals = Menu.getInstance().allMeals();
        
        for (Meal meal : orderedMeals)
            for (Meal menuMeal : menuMeals) {
                String descript = meal.recommendedWith(menuMeal);
                    if (descript != null)
                        result.put(menuMeal, descript);
                    if (result.size() == quantity)
                        return result;
                }
        return result;
    }
    
    //TODO : Add Order.getCheck() method.
    public double total(){
        double total =.0;
        Map<Meal, Integer> mealsQ = common.getMeals();
        for (Meal m : mealsQ.keySet()){
            total += mealsQ.get(m) * m.getPrice();
        }
        for (Person p : guests){
            mealsQ = p.getMeals();
            for (Meal m : mealsQ.keySet())
                total += mealsQ.get(m) * m.getPrice();
        }
        
        return total;
    }

    /**
     * 
     * @return distinct all meals in these order
     */
    public List<Meal> allMeals() {
        Set<Meal> mealsSet = new LinkedHashSet<Meal>();
        for (Person p : guests){
            for (Meal m : p){
                mealsSet.add(m);
            }
        }
        for (Meal m : common){
            mealsSet.add(m);
        }
        
        List<Meal> result = new LinkedList<Meal>(mealsSet);
        return result;
    }

    public int getMealsNumber(Meal meal) {
        int q =  common.mealQuantity(meal);
        for (Person p : guests){
            q += p.mealQuantity(meal);
        }
        return q;
    }

    public int deleteAllMeals(Meal theMeal) {
        int ret = common.takeAway(theMeal);
        for (Person p : guests){
            ret += p.takeAway(theMeal);
        }
        return ret;
    }

    public boolean reduceMeal(Meal theMeal) {
        boolean ret = false;
        if (common.containMeal(theMeal)){
            common.reduceMealNumber(theMeal);
            ret = true;
        } else {
            for (Person p : guests){
               if (p.containMeal(theMeal)){
                   p.reduceMealNumber(theMeal);
                   ret = true;
                   break;
               }
            }
            ret = false;
        }
        
        return ret;
    }
    
    
    /** Notifies all listeners;
     * 
     */
    public void update(){
        for (OrderListener listener : orderListeners){
            listener.onOrderUpdated();
        }
    }
    
    private List<OrderListener> orderListeners = new LinkedList<OrderListener>();
    
    public void addListener(OrderListener listener){
        if (!orderListeners.contains(listener)){
            orderListeners.add(listener);
        }
    }
    
    public void removeListener(OrderListener listener){
        if (orderListeners.contains(listener)){
            orderListeners.remove(listener);
        }
    }
    
}
